<?php //list_products.php

include_once "logic.php";
include_once "src/dist/html/layout_header.php";
require_once "vendor/autoload.php";

use ScandiwebTest\Objects\Category;

$category = new Category($conn);
$category_array = $category->read();

echo $html = <<<html
<div class="row underline">
    <div class="col-md-10">
        <h2>Product List</h2>
    </div>
    <div class="col-md-2">
        <input type="submit" name="go_to_product_add" form="list_products" value="Add">
        <input type="submit" name="mass_delete" form="list_products" value="Mass Delete">
    </div>
</div>
<form action='list_products.php' method='post' id="list_products">
html;

// Display 4 products per row
$numOfCols = 4;
$rowCount = 0;

$products = getProducts($conn, $category_array);

if ($products) {
    foreach ($products as $product) {
        $product_box = <<<EOD
        <div class="col-md-3">
            <div class="product-info">
                <input class = 'check' type='checkbox' name='check[]' value="$product->sku">
                <div class="product-name">
                    <small>{$product->sku}</small>
                    <h5>{$product->name}</h5>
                </div>
                <div class="product-price">
                    <h5>{$product->price}$</h5>
                </div>
                <div class="product-property">
                    <h5>{$product->showProperty()}</h5>
                </div>
            </div>
        </div>
    EOD;
        if ($rowCount % $numOfCols == 0) {
            echo '<div class="row">';
        }

        echo $product_box;
        $rowCount++;

        if ($rowCount % $numOfCols == 0) {
            echo '</div>';
        }
    }
    if ($rowCount % $numOfCols != 0) {
        echo '</div>';
    }
} else {
    echo "<div class='row alert alert-danger'>No products found.</div>";
}

require_once "src/dist/html/layout_footer.php";
