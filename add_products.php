<?php //add_products.php

include_once "logic.php";
require_once "vendor/autoload.php";
require_once "src/dist/html/layout_header.php";

use ScandiwebTest\Objects\Category;

// return from db categories
$category = new Category($conn);
$category = $category->read();

?>

<div class="row underline">
    <div class="col-md-10">
        <h2>Product Add</h2>
    </div>
    <div class="col-md-2">
        <input type="submit" name="save_product" form="add_product" value="Save">
        <input type="submit" name="go_to_product_list" form="add_product" value="Cancel">
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <form class="add_product" id="add_product" method="post" action="add_products.php">
            <div class="field" status="active"><label>SKU</label><input id="sku" type="text" name="sku"><span class="input_info">Please provide products SKU</span></div>
            <div class="field" status="active"><label>Name</label><input id="name" type="text" name="name"><span class="input_info">Please provide products name</span></div>
            <div class="field" status="active"><label>Price</label><input id="price" type="text" name="price"><span class="input_info">Please provide products price</span></div>
            <div class="field" status="active">
                <label>Type Switcher</label><select id="type" name="type">
                    <option value="" selected disabled hidden>Choose here</option>
                    <?php
                    // return from db categories and put it into select
                    foreach ($category as $row_category) {
                        echo "<option value='{$row_category['type_id']}'>{$row_category['type_name']}</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="type field" id="1" status="disabled" title="book"><label>Weight</label><input type="number" name="special_attribute[weight]"><span class="input_info">Please provide books weight in KG</span></div>
            <div class="type field" id="2" status="disabled" title="dvd"><label>Size</label><input type="number" name="special_attribute[size]"><span class="input_info">Please provide dvd size in MB</span></div>
            <div class="type" id="3" status="disabled" title="furniture">
                <div class="field"><label>Height (CM)</label><input name="special_attribute[height]" type="number"><span class="input_info">Please provide furniture height in CM</span></div>
                <div class="field"><label>Width  (CM)</label><input name="special_attribute[width]" type="number"><span class="input_info">Please provide furniture width in CM</span></div>
                <div class="field"><label>Length (CM)</label><input name="special_attribute[length]" type="number"><span class="input_info">Please provide furniture length in CM</span></div>
            </div>
        </form>
    </div>
    <div class="col-md-4 " >
        <form class="add_product">
            <div id="errorDiv" class="field errors"></div>
        </form>
    </div>
</div>

<?php
require_once "src/dist/html/layout_footer.php";
?>

