$(function () {

    $('#type').change(function () {
 // type switcher hide & show
        $('.type').hide();
        $('.type').attr('status', 'disabled');
        $('#' + this.value).show();
        $('#' + this.value).attr('status', 'active');
    });

    $('#add_product').submit(function () {

        let validation = true;

        if ($(document.activeElement).val() == 'Cancel') { //do not display alert on leaving the page
            return validation;
        }

        let input_arr = $('[status=active] :input');
        let error_messages = [];

        for (index=0; index < input_arr.length; ++index) {
            if (input_arr[index].value.trim() === '' || input_arr[index].value == null) {
                validation = false;
                //Prepare error output
                error_messages.push($(input_arr[index])[0].previousSibling.innerText + ' is required');

                // Highlight empty fields
                $(input_arr[index]).parent().addClass('highlight');
                setTimeout(function () {
                    for (index=0; index < input_arr.length; ++index) {
                        $(input_arr[index]).parent().removeClass('highlight');
                    }
                }, 1000);
            }
        }

        if (!$.isNumeric($('#price').val())) { // if price is not numeric value
            validation = false;
            error_messages.push('Price has to be numeric');
        } else if ($.isNumeric($('#price').val()) && $('#price').val() <= 0) { // if price is numeric value, but <0
            validation = false;
            error_messages.push('Price has to be positive');
        }

        if (error_messages.length > 0) { // Display error messages
            $('#errorDiv').text('');
            $('#errorDiv').append(error_messages.join(' <br> '));
            $('#errorDiv').show();
        }
        return validation;
    });
});
