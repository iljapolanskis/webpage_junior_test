<?php

namespace ScandiwebTest\Objects;

class Book extends Product
{
    public function getSpecialAttribute()
    {
        return $this->special_attribute['weight'];
    }

    public function showProperty()
    {
        return "Weight: " . $this->special_attribute ." KG";
    }
}
