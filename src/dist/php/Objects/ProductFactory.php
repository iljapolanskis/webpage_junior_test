<?php

namespace ScandiwebTest\Objects;

class ProductFactory
{
    public static function create($class, $db)
    {
        $class = "ScandiwebTest\\Objects\\".$class;
        return new $class($db);
    }
}
