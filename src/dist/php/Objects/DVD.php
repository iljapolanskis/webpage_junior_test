<?php

namespace ScandiwebTest\Objects;

class DVD extends Product
{
    public function getSpecialAttribute()
    {
        return $this->special_attribute['size'];
    }

    public function showProperty()
    {
        return "Size: " . $this->special_attribute ." MB";
    }
}
