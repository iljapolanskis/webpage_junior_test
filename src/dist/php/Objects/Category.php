<?php

namespace ScandiwebTest\Objects;

class Category
{
    const TABLE_NAME = "product_types";
    private $conn;

    public $types_array = null;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    //return from db all info on types
    public function read()
    {
        $query = "SELECT * FROM " . self::TABLE_NAME . " ORDER BY type_id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $this->types_array = $stmt -> fetchAll();
        return $this->types_array;
    }

    // return type_name by type_id
    public static function getName($types_array, $type_id)
    {
        if (is_null($types_array)) {
            return null;
        }
        foreach ($types_array as $key => $val) {
            if ($val['type_id'] == $type_id) {
                return $types_array[$key]['type_name'];
            }
        }
    }
}
