<?php

namespace ScandiwebTest\Objects;

abstract class Product
{
    const TABLE_NAME = "products";
    private $db;

    public $sku;
    public $name;
    public $price;
    public $type;
    public $special_attribute;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function setValues($sku, $name, $price, $type, $special_attribute)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->type = $type;
        $this->special_attribute = $special_attribute;
    }


    public static function readAll($conn)
    {
        $query = "SELECT * FROM " .self::TABLE_NAME. " WHERE 1";
        $stmt = $conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    public static function findBySKU($product_array, $sku)
    {
        if (is_null($product_array) || is_null($sku)) {
            return null;
        }
        foreach ($product_array as $key => $val) {
            if ($val->sku == $sku) {
                return $val;
            }
        }
    }

    public function deleteFromDB()
    {
        $query= "DELETE FROM ".self::TABLE_NAME." WHERE sku=:sku";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":sku", $this->sku);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function saveIntoDB()
    {
        $query = "INSERT INTO
                    " . self::TABLE_NAME . "
                SET
                    sku=:sku, name=:name, price=:price, type=:type, special_attribute=:special_attribute";
        $stmt = $this->db->prepare($query);

        $this->sku = $this->sanitizeString($this->sku);
        $this->name = $this->sanitizeString($this->name);
        $this->price = $this->sanitizeString($this->price);
        $this->type = $this->sanitizeString($this->type);

        $this->special_attribute = $this->getSpecialAttribute();
        $this->special_attribute = $this->sanitizeString($this->special_attribute);

        $stmt->bindParam(":sku", $this->sku);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":type", $this->type);
        $stmt->bindParam(":special_attribute", $this->special_attribute);

        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    // against HTML injections
    public function sanitizeString($var)
    {
        $var = stripcslashes($var);
        $var = strip_tags($var);
        $var = htmlentities($var);
        return $var;
    }

    abstract public function getSpecialAttribute();

    abstract public function showProperty();
}
