<?php

namespace ScandiwebTest\Objects;

class Database
{
    private $host = "localhost";
    private $db_name = "id16242503_shop";
    private $username = "id16242503_kau3ep";
    private $password = "at=k#6h]*pJE?TG";
    public $db;


    public function getConnection()
    {
        $this->db = null;
        try {
            $this->db = new \PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
        } catch (PDOException $exception) {
            echo "Connection Error: " . $exception->getMessage();
        }
        return $this->db;
    }
}
