<?php

namespace ScandiwebTest\Objects;

class Furniture extends Product
{
    public function getSpecialAttribute()
    {
        $this->prepareSpecialAttribute();
        return $this->special_attribute;
    }

    private function prepareSpecialAttribute()
    {
        $this->special_attribute =  $this->special_attribute['height'] . 'x' . $this->special_attribute['width'] . 'x' . $this->special_attribute['length'];
    }


    public function showProperty()
    {
        return "Dimensions: " . $this->special_attribute ." HxWxL";
    }
}
