<?php

use ScandiwebTest\Objects\Product;
use ScandiwebTest\Objects\ProductFactory;
use ScandiwebTest\Objects\Category;

// Delete all checked products
function massDelete($products)
{
    $sku_arr = $_POST['check'];
    foreach ($sku_arr as $row) {
        try {
            Product::findBySKU($products, $row)->deleteFromDB();
        } catch (\Throwable $t) { // in case of reload page after delete
            echo "<div class='alert alert-danger'>Product could not be deleted.</div>";
        }
    }
}

//returns from db an array of Product objects
function getProducts($db, $category_array)
{
    $result = Product::readAll($db);
    $products = [];

    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        $product = ProductFactory::create(Category::getName($category_array, $row['type']), $db);
        $product->setValues($row['sku'], $row['name'], $row['price'], $row['type'], $row['special_attribute']);
        $products[] = $product;
    }
    return $products;
}
