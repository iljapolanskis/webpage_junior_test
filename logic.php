<?php //logic.php

require_once "vendor/autoload.php";
require_once  "src/dist/php/functions.php";

use ScandiwebTest\Objects\Category;
use ScandiwebTest\Objects\Database;
use ScandiwebTest\Objects\ProductFactory;
use ScandiwebTest\Objects\Product;
use ScandiwebTest\Objects\DVD;
use ScandiwebTest\Objects\Book;
use ScandiwebTest\Objects\Furniture;

//Connect to MySQL db
$database = new Database();
$conn = $database->getConnection();

$category = new Category($conn);
$category_array = $category->read();

$products = getProducts($conn, $category_array);


if ($_POST) { // If form submitted
    // Save product to DB
    if (isset($_POST['save_product']) &&
        (isset($_POST['sku']) && !empty($_POST['sku'])) &&
        (isset($_POST['name']) && !empty($_POST['name']))&&
        (isset($_POST['price']) && !empty($_POST['price']))&&
        (isset($_POST['special_attribute']) && !empty($_POST['special_attribute']))&&
        (isset($_POST['type']) && !empty($_POST['type']))) {
        // product creation
        $product = ProductFactory::create(Category::getName($category_array, $_POST['type']), $conn);
        $product->setValues($_POST['sku'], $_POST['name'], $_POST['price'], $_POST['type'], $_POST['special_attribute']);

        // product save
        if ($product->saveIntoDB()) { // returns true on success
            echo "<div class='alert alert-success'>Product successfully added.</div>";
        } else { // returns false otherwise
            echo "<div class='alert alert-danger'>Product could not be added.</div>";
        }
    }
    // Mass delete as per checkboxes
    if (isset($_POST['mass_delete']) && isset($_POST['check'])) {
        massDelete($products);
    }
    // redirects to product add page
    if (isset($_POST['go_to_product_add'])) {
        header("Location: add_products.php");
    }
    // redirects to product add page
    if (isset($_POST['go_to_product_list'])) {
        header("Location: list_products.php");
    }
}
